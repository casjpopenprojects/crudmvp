package com.casjp.crudmvp.modules.home;

import android.util.Log;

import com.casjp.crudmvp.app.CrudMvpApplication;
import com.casjp.crudmvp.core.realm.dao.DaoCallback;
import com.casjp.crudmvp.core.realm.dao.TodoDao;
import com.casjp.crudmvp.core.realm.model.Todo;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public class MainPresenter implements Main.Presenter {

    public static final String TAG = MainPresenter.class.getSimpleName();

    Main.View view;
    DaoCallback.OnSaveCallback onSaveCallback;

    TodoDao todoDao;

    public MainPresenter() {
        todoDao = new TodoDao();
    }

    @Override
    public void attachView(Main.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void subscribeCallback() {

        onSaveCallback = new DaoCallback.OnSaveCallback() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "onSuccess: Added ##########");
            }

            @Override
            public void onError(String message) {
                view.onTodoEditTextError();
                Log.d(TAG, "onError: " + message);
            }
        };
    }

    @Override
    public void unsubscribeCallback() {
        onSaveCallback = null;
    }

    @Override
    public void save(String name) {
        long id = CrudMvpApplication.todoPrimaryKey.getAndIncrement();

        Todo todo = new Todo();
        todo.setId(id);
        todo.setName(name);
        if (name.isEmpty()) {
            view.onTodoEditTextError();
        } else {
            todoDao.save(todo, onSaveCallback);
        }
    }
}
