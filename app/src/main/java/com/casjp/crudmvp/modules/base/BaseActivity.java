package com.casjp.crudmvp.modules.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initComponents();
    }

    protected void initComponents() {
        setContentView(getLayoutResource());
    }

    protected abstract int getLayoutResource();
}
