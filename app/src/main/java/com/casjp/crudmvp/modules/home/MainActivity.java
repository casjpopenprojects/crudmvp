package com.casjp.crudmvp.modules.home;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;

import com.casjp.crudmvp.R;
import com.casjp.crudmvp.modules.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements Main.View {

    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.etTodoName) EditText etTodoName;

    @BindView(R.id.btnAdd) Button btnAdd;

    Main.Presenter presenter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void initComponents() {
        super.initComponents();
        ButterKnife.bind(this);
        setupToolbar();

        presenter = new MainPresenter();
        presenter.attachView(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.subscribeCallback();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.unsubscribeCallback();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();

    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
    }

    @OnClick(R.id.btnAdd)
    public void AddTodo() {
        presenter.save(etTodoName.getText().toString());
    }

    @Override
    public void onTodoEditTextError() {
        etTodoName.setError("Cannot be empty");
    }

    @Override
    public void onTodoEditTextSuccess() {
        Snackbar.make(findViewById(R.id.activity_main),
                "Todo Successfully added",
                Snackbar.LENGTH_SHORT).show();
    }
}
