package com.casjp.crudmvp.modules.base;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public interface Mvp {

    public interface Model {
        // all common to all model
    }

    public interface View {
        // all common to all view
    }

    public interface Presenter<V> {

        void attachView(V view);

        void detachView();

        void subscribeCallback();

        void unsubscribeCallback();

    }

}
