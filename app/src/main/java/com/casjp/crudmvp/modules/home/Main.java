package com.casjp.crudmvp.modules.home;

import com.casjp.crudmvp.core.realm.model.Todo;
import com.casjp.crudmvp.modules.base.Mvp;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public interface Main {

    interface Model extends Mvp.Model {

    }

    interface View extends Mvp.View {

        void onTodoEditTextError();

        void onTodoEditTextSuccess();

    }

    interface Presenter extends Mvp.Presenter<Main.View> {
        void save(String name);
    }

}
