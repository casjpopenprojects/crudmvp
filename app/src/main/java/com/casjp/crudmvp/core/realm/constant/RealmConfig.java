package com.casjp.crudmvp.core.realm.constant;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public class RealmConfig {

    public static final String DATABASE_NAME = "todo.realm";

    public static final int DATABASE_VERSION = 1;

}
