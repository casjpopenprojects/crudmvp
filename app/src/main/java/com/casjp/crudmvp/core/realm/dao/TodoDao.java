package com.casjp.crudmvp.core.realm.dao;

import com.casjp.crudmvp.core.realm.model.Todo;

import io.realm.RealmResults;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public class TodoDao extends AbstractDao<Todo>
        implements Dao<Todo> {


    public TodoDao() {
        super(Todo.class);
    }

    @Override
    public void save(Todo object, DaoCallback.OnSaveCallback onSaveCallback) {
        _insert(object, onSaveCallback);
    }

    @Override
    public Todo find(long id) {
        return null;
    }

    @Override
    public void findAll(DaoCallback.OnFindAllCallback<Todo> onFindAllCallback) {
         _findAll(onFindAllCallback);
    }
}
