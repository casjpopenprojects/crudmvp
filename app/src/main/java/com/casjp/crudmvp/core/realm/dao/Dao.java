package com.casjp.crudmvp.core.realm.dao;

import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public interface Dao<T extends RealmObject> {

    void save(T object, DaoCallback.OnSaveCallback onSaveCallback);

    T find(long id);

    void findAll(DaoCallback.OnFindAllCallback<T> onFindAllCallback);

}
