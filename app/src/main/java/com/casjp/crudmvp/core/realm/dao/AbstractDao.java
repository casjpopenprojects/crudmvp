package com.casjp.crudmvp.core.realm.dao;

import android.util.Log;

import com.casjp.crudmvp.app.CrudMvpApplication;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public abstract class AbstractDao<T extends RealmObject> {
    
    public static final String TAG = AbstractDao.class.getSimpleName();

    Class<T> clazz;

    public AbstractDao(Class<T> clazz) {
        this.clazz = clazz;
    }

    protected void _insert(final T object, final DaoCallback.OnSaveCallback callback) {
        Realm realm = Realm.getDefaultInstance();

        if (realm == null) realm.getInstance(CrudMvpApplication.getRealmConfigurationInstance());

        realm.executeTransactionAsync(new Realm.Transaction() {

            @Override
            public void execute(Realm realm) {
                T realmObject = realm.copyToRealm(object);
            }

        }, new Realm.Transaction.OnSuccess() {

            @Override
            public void onSuccess() {
                Log.d(TAG, "onSuccess: was call here ");
                callback.onSuccess();
            }
        }, new Realm.Transaction.OnError() {

            @Override
            public void onError(Throwable error) {
                Log.d(TAG, "onError: was call here");
                callback.onError("Error occur while adding");
            }
        });
    }

    protected void _findAll(DaoCallback.OnFindAllCallback<T> callback) {
        Realm realm = Realm.getDefaultInstance();

        if (realm == null) Realm.getInstance(CrudMvpApplication.getRealmConfigurationInstance());

        RealmResults<T> realmResults = realm.where(clazz).findAllAsync();
        if (realmResults.size() > 0) {
            callback.onSuccess(realmResults);
        } else {
            callback.onError("No Data Find");
        }
    }

}
