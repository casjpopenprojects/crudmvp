package com.casjp.crudmvp.core.realm.dao;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public interface DaoCallback {

    interface OnSaveCallback {
        void onSuccess();
        void onError(String message);
    }

    interface OnDeleteCallback {
        void onSuccess();
        void onError(String message);
    }

    interface OnFindCallback<T extends RealmObject> {
        void onSuccess(T objects);
        void onError(String message);
    }

    interface OnFindAllCallback<T extends RealmObject> {
        void onSuccess(RealmResults<T> objects);
        void onError(String message);
    }

}
