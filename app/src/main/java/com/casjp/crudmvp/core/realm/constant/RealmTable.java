package com.casjp.crudmvp.core.realm.constant;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public interface RealmTable {


    interface Todo {
        String ID = "id";
        String NAME = "name";
    }

}
