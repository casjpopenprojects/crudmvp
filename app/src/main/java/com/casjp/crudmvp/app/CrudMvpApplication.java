package com.casjp.crudmvp.app;

import android.app.Application;

import com.casjp.crudmvp.core.realm.constant.RealmConfig;
import com.casjp.crudmvp.core.realm.constant.RealmTable;
import com.casjp.crudmvp.core.realm.model.Todo;

import java.util.concurrent.atomic.AtomicLong;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by John Paul Cas on 3/12/2017.
 */

public class CrudMvpApplication extends Application {

    private static CrudMvpApplication instance;
    private static RealmConfiguration config;

    public static AtomicLong todoPrimaryKey;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initRealm();
    }

    private void initRealm() {
        Realm.init(this);
        config = new RealmConfiguration.Builder()
                .name(RealmConfig.DATABASE_NAME)
                .schemaVersion(RealmConfig.DATABASE_VERSION)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);

        Realm realm = Realm.getDefaultInstance();

        try {

            todoPrimaryKey = new AtomicLong(realm.where(Todo.class).max(RealmTable.Todo.ID).longValue() + 1);
        } catch (Exception e) {
            realm.beginTransaction();

            Todo todo = new Todo();
            todo.setId(0);
            todo.setName("CrudMvp");
            realm.copyToRealm(todo);
            todoPrimaryKey = new AtomicLong(realm.where(Todo.class).max(RealmTable.Todo.ID).longValue() + 1);

            RealmResults<Todo> todos = realm.where(Todo.class).equalTo(RealmTable.Todo.ID, 1).findAll();
            todos.deleteAllFromRealm();
            realm.commitTransaction();

            realm.close();
        }

    }

    public static CrudMvpApplication getInstance() {
        return instance;
    }

    public static RealmConfiguration getRealmConfigurationInstance() {
        return config;
    }
}
